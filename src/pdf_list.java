/*
 * fi_pdf.java
 * Created on 04-Aug-2010, 11:38:14
 */
import java.io.BufferedReader;
import java.io.File;
import java.util.*;
import com.edi.common.Logging;
import com.edi.common.MySqlConnection.MySqlConnection;
import com.edi.common.dates;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class pdf_list extends javax.swing.JFrame {
    
    private Logging   log = new Logging("Automation");
    private MySqlConnection mysql = null;
    private Connection conn = null;
    private int bAutoRun = 0;
    private String ErrCode = "0";
    private String SourcePath = "";
    private String ArchivePath = "";
    private String ReturnPath = "";
    private String SessionPath = "";
    private String LogFilePath = "";
    private String tbl = "";
    private String db = "";
    private ArrayList flist = new ArrayList();
    private ArrayList fldlist = new ArrayList();
    private String masterfile = "";
    private String masterfilenm = "";
    private String acttime = "";
    private int write = 0;
    private String DBConfigFile = "O:\\AUTO\\Configs\\DbServers.cfg";

    /** Creates new form fi_pdf */
    public pdf_list(String[] args){
        // Set Look and Feel to Native style.
        try {
	    javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
	} catch (Exception e){
            //System.out.println(e);
	}  
        initComponents();
        try{

            if(args.length<=0){
                System.out.println("No Parameter found");
                exitForm(Integer.parseInt(ErrCode));
            }
            
            for(int f=0;f<args.length;f++){
                if(args[f].equalsIgnoreCase("-m")){
                    bAutoRun = 1;
                }else if(args[f].equalsIgnoreCase("-a")){
                    bt_click.setEnabled(false);
                    bAutoRun = 2;
                }else if(args[f].equalsIgnoreCase("-s")){
                   //Getting the connection details
                   GetConn(args[f+1]);
                }else if(args[f].equalsIgnoreCase("-db")){
                    db = args[f+1];
                }else if(args[f].equalsIgnoreCase("-tbl")){
                    tbl = args[f+1];
                }else if(args[f].equalsIgnoreCase("-sp")){
                    SourcePath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-l")){
                    LogFilePath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-ap")){
                    ArchivePath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-rp")){
                    ReturnPath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-mp")){
                    SessionPath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-sf")){
                    DBConfigFile = args[f+1];
                }
            }

            setVisible(true);

            if (bAutoRun==2){
                bt_clickActionPerformed(null);
                exitForm(0);
            }
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"Exe Parameter Failed: " + e,"Invalid Exe Paramenter",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("Exe Parameter Failed: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
        }

    }

    private void GetConn(String svr){
        try{
            String [] connDetail = GetConnDetail(svr);
            mysql = new MySqlConnection(connDetail[0],connDetail[1],db,connDetail[2]);
            conn = mysql.getConnection();
       }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"OpenDBConn: " + e.getMessage());
            }else{
                log.append("OpenDBConn: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
       }
    }

    private String [] GetConnDetail(String svr){
      try{
          String [] detail = new String[3];
          String [] tvalues = null;
           File sourcefile = new File(DBConfigFile);
            if(sourcefile.exists()){
                BufferedReader in = new BufferedReader(new FileReader(sourcefile));
                String str = null;
                while ((str = in.readLine()) != null) {
                    //if(str.indexOf(svr)>=0)
                    tvalues = str.split("\t");
                    if(tvalues[0].compareToIgnoreCase(svr)==0){
                        detail[0] = tvalues[2];
                        detail[1] = tvalues[3];
                        detail[2] = tvalues[4];
                    }
                }
                in.close();
            } else{
                log.append("Could not found the config file: O:\\AUTO\\Configs\\DbServers.cfg",  LogFilePath, "pdf_sort");
            }

          return detail;
      }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetConnDetail: " + e.getMessage());
            }else{
                log.append("GetConnDetail: " + e.getMessage(),  LogFilePath, "PDF_Sort");
            }
            return null;
       }
  }

    private void exitForm(int code) {
       if(code == 0)
            dispose();
       else
           System.exit(1);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new javax.swing.JPanel();
        lbl_file = new javax.swing.JLabel();
        lbl_folder = new javax.swing.JLabel();
        bt_click = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PDF Sort");
        setResizable(false);

        lbl_file.setFont(new java.awt.Font("Tahoma", 1, 11));
        lbl_file.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbl_folder.setFont(new java.awt.Font("Tahoma", 1, 11));
        lbl_folder.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        bt_click.setText("click");
        bt_click.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_clickActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbl_folder, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE)
                    .addComponent(lbl_file, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bt_click)
                .addContainerGap())
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt_click, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(lbl_folder, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_file, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bt_clickActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_clickActionPerformed
        try{
            lbl_folder.setText("Finding list of folders");
            Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
            fldlist.clear();
            if(getFolders(SourcePath)){
                if(fldlist.size() > 0){
                    Collections.sort(fldlist); 
                    for(int f1=0;f1<fldlist.size();f1++){
                        lbl_folder.setText("Processing " + SourcePath + "\\" + fldlist.get(f1).toString() + "\\");
                        Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                        String filepath = SourcePath + "\\" + fldlist.get(f1).toString() + "\\";
                        lbl_file.setText("Finding list of files");
                        Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                        flist.clear();
                        if(getFiles(filepath)){
                            if(flist.size() > 0){
                                acttime = new dates().getJapDate(new java.util.Date());                                                               
                                for(int f=0;f<flist.size();f++){
                                    lbl_file.setText("Processing " + filepath + flist.get(f).toString());
                                    Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                                    insert(flist.get(f).toString());
                                }
                            }                                        
                        }                                
                    }                        
                 }
             }                            
             lbl_folder.setText("Finished Processing");
             Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"bt_clickActionPerformed: " + e.getMessage(),"bt_clickActionPerformed",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("bt_clickActionPerformed: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
    }//GEN-LAST:event_bt_clickActionPerformed

    private void movesessionfile(){
        try{
            File msf = new File(masterfile);
            File mdf = new File(SessionPath + "\\" +  masterfilenm);
            msf.renameTo(mdf);
            mdf = null;
            msf = null;
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"movesessionfile: " + e.getMessage(),"movesessionfile",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("movesessionfile: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
    }

    private void CreateReturnCSV(String [] rec, String comment){
        try{
            String[] fname = masterfile.split("\\\\");
            String fnm = fname[fname.length-1].replace("PDF","BAD");
            BufferedWriter out = new BufferedWriter(new FileWriter(ReturnPath + "\\" + fnm, true));
            out.write(rec[0].trim() + ",\"" + rec[1].trim() + "\"," + rec[2].trim() + ",\"" + rec[3].trim() + "\",\"" + comment.trim() + "\"\n");
            out.close();
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"CreateReturnCSV: " + e.getMessage(),"CreateReturnCSV",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("CreateReturnCSV: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
    }

    private boolean insert(String file){
        try{
            Statement ist = conn.createStatement();           
            System.out.println("INSERT INTO " + db + "." + tbl + "(filename) VALUES('" + file + "')");
            ist.executeUpdate("INSERT INTO " + db + "." + tbl + "(filename) VALUES('" + file + "')");
            ist.close();
            return true;
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"insert: " + e.getMessage(),"insert",javax.swing.JOptionPane.ERROR_MESSAGE);                
            }else{
                log.append("insert: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return false;
        }
    }

    private boolean update(String[] rec, String filepath){
        try{
            Statement ust = conn.createStatement();
//           ust.executeUpdate("UPDATE " + db + "." + tbl + " SET secid  = '" + rec[0] + "', " +
//                              " actflag = 'U', " +
//                              " acttime  = '" + acttime + "', " +
//                              " file_path  = '" + filepath + "', " +
//                              " file_name  = '" + rec[1] + "', " +
//                              " file_size  = '" + rec[2] + "'" +
//                              " WHERE SecID = '" + rec[0] + "'" +
//                              " and file_name = '" + rec[1] + "'");
            ust.close();
            return true;
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"update: " + e.getMessage(),"update",javax.swing.JOptionPane.ERROR_MESSAGE);
            }else{
                log.append("update: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return false;
        }
    }

    private boolean delupdate(String[] rec, String filepath){
        try{
            Statement ust = conn.createStatement();
            ust.executeUpdate("UPDATE " + db + "." + tbl + " SET secid  = '" + rec[0] + "', " +
                              " actflag = 'D', " +
                              " acttime  = '" + acttime + "', " +
                              " file_path  = '" + filepath + "', " +
                              " file_name  = '" + rec[1] + "', " +
                              " file_size  = '" + rec[2] + "'" +
                              " WHERE SecID = '" + rec[0] + "'" +
                              " and file_name = '" + rec[1] + "'");
            ust.close();
            return true;
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"update: " + e.getMessage(),"update",javax.swing.JOptionPane.ERROR_MESSAGE);
            }else{
                log.append("update: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return false;
        }
    }

    private String ChkUpdateInsert(String[] rec){
        try{
            String rtn = "";
            boolean isupdate = false;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + db + "." + tbl + " WHERE SecID = '" + rec[0] +
                                             "' and file_name = '" + rec[1] + "'");
            if(rs!=null){
                if(rs.next()){
                    rtn = "update, " + rs.getString("file_size");
                    isupdate = true;
                }
                
            }

            if(!isupdate){
                rtn = "insert, ";
            }
            stmt.close();
            rs.close();
            return rtn;
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"ChkUpdateInsert: " + e.getMessage(),"bt_clickActionPerformed",javax.swing.JOptionPane.ERROR_MESSAGE);
            }else{
                log.append("ChkUpdateInsert: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return "";
        }
    }

    private boolean isDirExists(String fname){
        try{
            String parent =  (new File(fname).getParent());
            System.out.println(parent);
            File dir = new File(parent);
            if(dir.exists()==false){
                return dir.mkdir();
            }else{
                return true;
            }
        }catch(Exception e){
            return false;
        }
    }


    private void ChkFinalFiles(String fld){
        try{
            ArrayList chkfiles = new ArrayList();
            String[] DirContents = null;
            File DirectoryListing = new File(fld);
            DirContents = DirectoryListing.list();
            for (int x=0; x<DirContents.length; x++){
                DirContents[x] = DirContents[x].toUpperCase();
            }

            for (int x = 0; x<DirContents.length; x++){
                // Dose File start with filePrefix
                 if ( DirContents[x].toUpperCase().endsWith("PDF") == true)
                     chkfiles.add(DirContents[x]);
            }
            if(chkfiles.size()>0){
                for(int f=0;f<chkfiles.size();f++){
                    File sf = new File(fld + chkfiles.get(f).toString());
                    long len = sf.length();
                    File df = new File(ReturnPath + "\\" +  chkfiles.get(f).toString());
                    sf.renameTo(df);
                    df = null;
                    sf = null;
                    String [] rtn = new String[4];
                    rtn[0] = "";
                    rtn[1] = chkfiles.get(f).toString();
                    rtn[2] = String.valueOf(len);
                    rtn[3] = "";
                    CreateReturnCSV(rtn, "Orphan File");
                }
            }
        }catch(Exception e){
        }
    }

    private boolean getFiles(String fld){
        try{
            boolean rtn = false;
            String[] DirContents = null;            
            File DirectoryListing = new File(fld);
            DirContents = DirectoryListing.list();
            for (int x=0; x<DirContents.length; x++){
                DirContents[x] = DirContents[x].toUpperCase();
            }

            for (int x = 0; x<DirContents.length; x++){
                // Dose File start with filePrefix                
                /* if ( DirContents[x].toUpperCase().endsWith("CSV") == true){
                     masterfile = fld + DirContents[x];
                     masterfilenm = DirContents[x];
                 }else{*/
                     flist.add(DirContents[x]);
                 //}
            }
            if(flist.size()>0) rtn = true;
	    return rtn;
        }catch(Exception e){
            return false;
        }
    }

    private boolean getFolders(String fld){
        try{
            boolean rtn = false;
            String[] DirContents = null;
            File DirectoryListing = new File(fld);
            DirContents = DirectoryListing.list();
            for (int x=0; x<DirContents.length; x++){
                DirContents[x] = DirContents[x].toUpperCase();
            }

            for (int x = 0; x<DirContents.length; x++){
                 fldlist.add(DirContents[x]);
            }
            if(fldlist.size()>0) rtn = true;
	    return rtn;
        }catch(Exception e){
            return false;
        }
    }

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
       new pdf_list(args);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel;
    private javax.swing.JButton bt_click;
    private javax.swing.JLabel lbl_file;
    private javax.swing.JLabel lbl_folder;
    // End of variables declaration//GEN-END:variables

}
