
import com.edi.common.Logging;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * * @author h.patel
 */

public class FTPTransfer extends FTPClient{

    private Logging   log = new Logging("Automation");
    private String site = "";
    private boolean crccheck = false;
    private String[] serverconf;
    private boolean wasError = false;
    private boolean CRCError = false;
   
    /* the following vars should be populated using
     * the FTPServer.cfg configuration file, base on
     * the site parameter passed in the command line
     */
    private String host = "";
    private String user = "";
    private String pass = "";
    private int port = 21;
    private String filename = "";

    /* the following vars should be populated by the
     * praseCommandLine
     */
    private String logfile = "";
    private String localfile = "";
    private String remotepath = "";
    private progress prog = new progress();    

    public boolean FTPTrans(String args[]) {

        try {
            praseCommandLine(args);
            File checkLocalFile = new File(localfile);
            if (!checkLocalFile.exists()) {
                log.append("The local file '" + localfile + "' does not exist",  logfile, "pdf_FTPTrans");
                //System.exit(1);
                return false;
            }
            /**/
            prog.setTitle(this.host);

            MessageListener msgl1 = new MessageListener() ;
            msgl1.setDisplayComp(prog.progressText_Label1);
            this.setMessageListener( msgl1 );
            this.setProgressMonitor( new ProgressMonitor() );

            prog.setVisible(true);
            String command="";
            prog.setProgressText2("[]<--\t"+command);
            prog.setProgressText("Setting up, Please wait...");
            prog.progressText_Label1.setText("[]<--\t"+this.host);
            this.setTransferBufferSize(10);

            
                // set up connection details and connect to server
                prog.setProgressText("Connecting to remote FTP server");
                setRemoteHost(host);
                setConnectMode(com.enterprisedt.net.ftp.FTPConnectMode.ACTIVE);
                connect();
                prog.setProgressText("Connected to remote FTP server");
                // Login to remote FTP server
                login(user, pass);
                prog.setProgressText("Loged in into remote FTP server");                
                // change directory
                try{
                    chdir(remotepath);
                }catch(Exception e){
                    String[] sdir = remotepath.split("\\\\");
                    try{
                        chdir(sdir[1]);
                        mkdir(sdir[2]);
                        chdir(remotepath);
                    }catch(Exception e1){
                        mkdir(sdir[1]);
                        chdir(sdir[1]);
                        mkdir(sdir[2]);
                        chdir(remotepath);
                    }
                    
                } 
                prog.setProgressText("changed Dir in remote FTP server");


                // Get the LOCAL directory file list for the path
                // specified in the command line arguments.               
                checkLocalFile = null;
                prog.setProgressText("Building upload file for " + localfile + ", Please wait...");
                File LocalFile = new File(localfile);

                // Loop thought each of the file in the localFile array
                
                   // String fname = localFiles[i].toString().substring(localFiles[i].toString().lastIndexOf("\\")+1,localFiles[i].toString().length());

                    // if the element within localFiles[] is not
                    // a directory and is a file then we will process
                    // the item as a file.
                    if (!LocalFile.isDirectory() && LocalFile.isFile()) {
                        
                        boolean wasFileUploaded = false;

                        String t = LocalFile.getName();
                       // t = t.substring(this.localpath.length());
                        prog.setProgressText("Uploading '" + LocalFile.toString() + "'");
                        if (getFileType(LocalFile.toString())){
                             setType(transferType.BINARY);
                        }else{
                             setType(transferType.ASCII);
                        }
                        put(LocalFile.toString(), remotepath + t,false);
                        wasFileUploaded = true;

                        //System.out.println(localFiles[i].toString().substring(localFiles[i].toString().lastIndexOf("\\")+1,localFiles[i].toString().length()));

                        // Do we need to check the CRC values of the two files?
                        CRCError = false;
                        if (this.crccheck == true) {
                            if (wasFileUploaded == true) {
                                // Get the Local and Remote CRC values
                                prog.setProgressText("Validating uploaded file...");
                                String localCRC = getCRC( LocalFile );
                                String remoteCRC = quote("XCRC " + t + "", null);
                                //System.out.println(remoteCRC);
                                // Convert the localCRC value from Long to Hex
                                Long l = Long.parseLong(localCRC);
                                localCRC = Long.toHexString(l).toUpperCase();
                                //System.out.println(localCRC);
                                // Compare the two CRC values

                                if (localCRC.compareToIgnoreCase(remoteCRC) != 0) {
                                    prog.setProgressText("Uploaded file '" + LocalFile + "' is invalid");
                                    log.append("CRC Failure: Site--> " + site + ", LocalFile--> " + LocalFile.toString() + ", RemotePath--> " + remotepath,  logfile, "pdf_FTPTrans");
                                    wasError = true;
                                    CRCError = true;
                                }else{                                    
                                    CRCError = false;
                                    wasError = false;
                                }
                            }else{
                                wasError = true;                                
                                log.append("File could not uploaded Site--> " + site + ", LocalPath--> " + LocalFile.toString() + ", RemotePath--> " + remotepath,  logfile, "pdf_FTPTrans");
                            }
                        }else{
                                if(!wasFileUploaded){
                                    wasError = true;                                    
                                    log.append("File could not uploaded Site--> " + site + ", LocalPath--> " + LocalFile.toString() + ", RemotePath--> " + remotepath,  logfile, "pdf_FTPTrans");
                                    
                                }else{
                                    wasError = false;
                                }
                        }

                    }
                    
                // Disconnect from remote FTP server
                prog.setProgressText("Disconnecting from FTP Server...");
                quit();

                // display finished message and wait for 3 seconds
                prog.setProgressText("FINISHED!");
                Thread.sleep(3000);
                System.gc();
                if (wasError == true && CRCError == true) {
                    prog.setVisible(false);
                    return false;
                }else if (wasError == true && CRCError == false) {
                    prog.setVisible(false);
                    return false;
                } else {
                    prog.setVisible(false);
                    return true;
                }

            } catch (InterruptedException ex) {
                //System.out.println(ex.getMessage());                
                log.append("InterupedException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localfile + ", RemotePath--> " + remotepath,  logfile, "pdf_FTPTrans");
                //System.exit(1);
                System.gc();
                return false;
            } catch (IOException ex) {
                //System.out.println(ex.getMessage())                
                log.append("IOException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localfile + ", RemotePath--> " + remotepath,  logfile, "pdf_FTPTrans");
                prog.progressText_Label1.setText("IOException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localfile + ", RemotePath--> " + remotepath);
                //System.exit(1);
                System.gc();
                return false;
            } catch (FTPException ex) {
                //System.out.println(ex.getMessage());                
                log.append("FTPException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localfile + ", RemotePath--> " + remotepath,  logfile, "pdf_FTPTrans");
                prog.progressText_Label1.setText("FTPException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localfile + ", RemotePath--> " + remotepath);
                //System.exit(1);
                System.gc();
                return false;
            }catch (Exception ex) {
                //System.out.println(ex.getMessage());                
                log.append("Exception: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localfile + ", RemotePath--> " + remotepath,  logfile, "pdf_FTPTrans");
                //System.exit(1);
                System.gc();
                return false;
            }            

    }

    

    private boolean getFileType(String fname){
    fname = fname.toLowerCase();
    if (fname.endsWith(".zip"))
        return true;
    if (fname.endsWith(".exe"))
        return true;
    if (fname.endsWith(".pdf"))
        return true;
    if (fname.endsWith(".z01"))
        return true;
    if (fname.endsWith(".z02"))
        return true;
    if (fname.endsWith(".z03"))
        return true;
    if (fname.endsWith(".tar"))
        return true;
    if (fname.endsWith(".gz"))
        return true;
    if (fname.endsWith(".xls"))
        return true;
    if (fname.endsWith(".doc"))
        return true;
    if (fname.endsWith(".jpg"))
        return true;
    if (fname.endsWith(".tif"))
        return true;
    if (fname.endsWith(".gif"))
        return true;

    //check for extension between 0 to 999
    try{
        String [] tmpext = fname.split("\\.");
        if(Integer.parseInt(tmpext[tmpext.length-1]) > 0 && Integer.parseInt(tmpext[tmpext.length-1]) <= 999){
            return true;
        }
    }catch(Exception e){
    }

    return false;
}

 private void praseCommandLine(String[] args) {
        if (args.length == 0) {
            log.append("Parameters not found",  logfile, "pdf_FTPTrans");
        }

        int chk = 0;
        for (int i = 0; i < args.length; i++) {

            if (args[i].compareToIgnoreCase("-site") == 0) {
                this.site = args[i + 1];

                // read ftp site config
                loadFTPConfig();
                for (int x = 1; x < serverconf.length; x++) {
                    if (serverconf[x].startsWith(this.site.toUpperCase())) {
                        // do something here
                        //appendLog("Found the site configuration line in FTPServer.cfg");

                        String temp[] = serverconf[x].split("\t");
                        this.host = temp[1];
                        this.user = temp[2];
                        this.pass = temp[3];
                        break;
                    }
                }
            }

            if (args[i].compareToIgnoreCase("-localfile") == 0) {
                this.localfile = args[i + 1];
            }                       

            if (args[i].compareToIgnoreCase("-remote") == 0) {
                this.remotepath = args[i + 1];
            }
                       

            if (args[i].compareToIgnoreCase("-XCRC") == 0) {
                if (args[i + 1].compareToIgnoreCase("ON") == 0) {
                    this.crccheck = true;
                } else {
                    this.crccheck = false;
                }
            }

            if (args[i].compareToIgnoreCase("-port") == 0) {
                try {
                    this.port = Integer.parseInt(args[i + 1]);
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    //System.exit(1);
                }
            }

            if (args[i].compareToIgnoreCase("-log") == 0) {
                try {
                    this.logfile = args[i + 1];
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    //System.exit(1);
                }
            }

        }        
    }

 private void loadFTPConfig() {
        try {
            BufferedReader in = new BufferedReader(new FileReader("J:\\java\\Prog\\h.patel\\PDF_Sort\\FTPServers.cfg"));
            String str;
            int linecount = 0;
            while ((str = in.readLine()) != null) {
                linecount++;
                //serverconf[xxx] = str;

            }
            serverconf = new String[linecount + 1];
            in.close();

        } catch (IOException e) {
        }

        try {
            BufferedReader in = new BufferedReader(new FileReader("J:\\java\\Prog\\h.patel\\PDF_Sort\\FTPServers.cfg"));
            String str;
            int currentline = 0;
            while ((str = in.readLine()) != null) {
                currentline++;
                serverconf[currentline] = str;
            }
            in.close();

        } catch (IOException e) {
        }


    }

 private String getCRC(File fileName) {
        try {
            CheckedInputStream cis = null;
            //long filesize = 0;
            // Computer CRC32 checksum
            cis = new CheckedInputStream(new FileInputStream(fileName.toString()), new CRC32() );
            //filesize = fileName.length();

            byte[] buf = new byte[128];
            while ( cis.read(buf)>=0) {
            }

            long checksum = cis.getChecksum().getValue();
           // System.out.println(checksum + " " + filesize + " " + fileName);

            return String.valueOf(checksum);

        } catch (Exception e) {
            log.append("getCRC: " + e.getMessage(),  logfile, "pdf_FTPTrans");
            //e.printStackTrace();
        }
        return null;
    }

}
