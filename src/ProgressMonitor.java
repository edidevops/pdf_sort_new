/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.enterprisedt.net.ftp.FTPProgressMonitor;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 *
 * @author i.cornish
 */
public class ProgressMonitor implements FTPProgressMonitor {

    public void bytesTransferred(long count) {
        //System.err.println("---> \t"+count+" bytes transfered");

        if (count < 1) {
            //JOptionPane.showMessageDialog(null, "Not Transfered!!!!");
            //appendLog("File Not Transfered!!!!");
        } else {
            //JOptionPane.showMessageDialog(null, "Transfered succesful");
            //appendLog("File Transfered succesfully");
        }
    }

    private void appendLog(String logMessage) {
        try {
            // Build DateTime String
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    java.util.Date now = new java.util.Date();
	    String sDate= sdf.format(now);
            BufferedWriter out = new BufferedWriter(new FileWriter("O:\\AUTO\\logs\\OpsFtp.log", true));
            out.write(sDate + "\t" + logMessage + "\r\n");
            out.close();
        } catch (IOException e) {
        }
    }
}
