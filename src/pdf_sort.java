/*
 * fi_pdf.java
 * Created on 04-Aug-2010, 11:38:14
 */
import com.artofsolving.jodconverter.DefaultDocumentFormatRegistry;
import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.DocumentFormat;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
import java.io.BufferedReader;
import java.io.File;
import java.util.*;
import com.edi.common.Logging;
import com.edi.common.MySqlConnection.MySqlConnection;
import com.edi.common.dates;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class pdf_sort extends javax.swing.JFrame {
    
    private Logging   log = new Logging("Automation");
    private MySqlConnection mysql = null;
    private Connection conn = null;
    private int bAutoRun = 0;
    private String ErrCode = "0";
    private String SourcePath = "";
    private String ArchivePath = "";
    private String CopyPath = "";
    private String ReturnPath = "";
    private String SessionPath = "";
    private String LogFilePath = "";
    private String tbl = "";
    private String db = "";
    private ArrayList flist = new ArrayList();
    private ArrayList fldlist = new ArrayList();
    private String masterfile = "";
    private String masterfilenm = "";
    private String acttime = "";
    private int write = 0;
    private String DBConfigFile = "J:\\java\\Prog\\h.patel\\PDF_Sort\\DbServers.cfg";
    private OpenOfficeConnection ooConn;

    /** Creates new form fi_pdf */
    public pdf_sort(String[] args){
        // Set Look and Feel to Native style.
        try {
	    javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
	} catch (Exception e){
            //System.out.println(e);
	}  
        initComponents();
        try{

            if(args.length<=0){
                System.out.println("No Parameter found");
                exitForm(Integer.parseInt(ErrCode));
            }
            
            for(int f=0;f<args.length;f++){
                if(args[f].equalsIgnoreCase("-m")){
                    bAutoRun = 1;
                }else if(args[f].equalsIgnoreCase("-a")){
                    bt_click.setEnabled(false);
                    bAutoRun = 2;
                }else if(args[f].equalsIgnoreCase("-s")){
                   //Getting the connection details
                   if(!GetConn(args[f+1])) exitForm(1);
                }else if(args[f].equalsIgnoreCase("-db")){
                    db = args[f+1];
                }else if(args[f].equalsIgnoreCase("-tbl")){
                    tbl = args[f+1];
                }else if(args[f].equalsIgnoreCase("-sp")){
                    SourcePath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-l")){
                    LogFilePath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-ap")){
                    ArchivePath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-rp")){
                    ReturnPath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-mp")){
                    SessionPath = args[f+1];
                }else if(args[f].equalsIgnoreCase("-sf")){
                    DBConfigFile = args[f+1];
                }else if(args[f].equalsIgnoreCase("-cp")){
                    CopyPath = args[f+1];
                }
            }

            setVisible(true);

            if (bAutoRun==2){
                bt_clickActionPerformed(null);
                exitForm(0);
            }
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"Exe Parameter Failed: " + e,"Invalid Exe Paramenter",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("Exe Parameter Failed: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
        }

    }

    private boolean GetConn(String svr){
        try{
            String [] connDetail = GetConnDetail(svr);
            mysql = new MySqlConnection(connDetail[0],connDetail[1],db,connDetail[2]);
            conn = mysql.getConnection();
            if(conn == null) return false;
            else return true;
       }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"OpenDBConn: " + e.getMessage());
            }else{
                log.append("OpenDBConn: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return false;
       }
    }

    private String [] GetConnDetail(String svr){
      try{
          String [] detail = new String[3];
          String [] tvalues = null;
           File sourcefile = new File(DBConfigFile);
            if(sourcefile.exists()){
                BufferedReader in = new BufferedReader(new FileReader(sourcefile));
                String str = null;
                while ((str = in.readLine()) != null) {
                    //if(str.indexOf(svr)>=0)
                    tvalues = str.split("\t");
                    if(tvalues[0].compareToIgnoreCase(svr)==0){
                        detail[0] = tvalues[2];
                        detail[1] = tvalues[3];
                        detail[2] = tvalues[4];
                    }
                }
                in.close();
            } else{
                log.append("Could not found the config file: O:\\AUTO\\Configs\\DbServers.cfg",  LogFilePath, "pdf_sort");
            }

          return detail;
      }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetConnDetail: " + e.getMessage());
            }else{
                log.append("GetConnDetail: " + e.getMessage(),  LogFilePath, "PDF_Sort");
            }
            return null;
       }
  }

    private void exitForm(int code) {
       if(code == 0)
            dispose();
       else
           System.exit(1);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new javax.swing.JPanel();
        lbl_file = new javax.swing.JLabel();
        lbl_folder = new javax.swing.JLabel();
        bt_click = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PDF Sort");
        setResizable(false);

        lbl_file.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_file.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbl_folder.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_folder.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        bt_click.setText("click");
        bt_click.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_clickActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbl_folder, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE)
                    .addComponent(lbl_file, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bt_click)
                .addContainerGap())
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt_click, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(lbl_folder, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_file, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(0, Short.MAX_VALUE)
                .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bt_clickActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_clickActionPerformed
        try{
           // ConvertToPDF("C:\\test\\tmp\\US071707AD51_PR_EN_1.DOC", "C:\\test\\ecodata\\");
            

            
            lbl_folder.setText("Finding list of folders");
            Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
           // if(del_web_upload(ArchivePath)){
            if(getFolders(SourcePath)){
                if(fldlist.size() > 0){
                    Collections.sort(fldlist); 
                    for(int f1=0;f1<fldlist.size();f1++){
                        lbl_folder.setText("Processing " + SourcePath + "\\" + fldlist.get(f1).toString() + "\\");
                        Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                        String filepath = SourcePath + "\\" + fldlist.get(f1).toString() + "\\";
                        lbl_file.setText("Finding list of files");
                        Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                        //if(getFiles(filepath)){
                           // if(flist.size() > 0){
                        if(getFiles(filepath)){
                        
                                acttime = new dates().getJapDate(new java.util.Date());
                                BufferedReader in = new BufferedReader(new FileReader(masterfile));
                                String str = null;
                                //in.readLine();
                                while((str = in.readLine()) != null){
                                    String [] tmp = str.split("\\|",11);
                                    for(int i=0;i<tmp.length;i++){
                                        tmp[i] = tmp[i].trim();
                                    }
                                    if(tmp!=null){
                                        int isFileExists = 0;
                                        /*if(tmp[4].trim().toUpperCase().equals("D")){
                                            delupdate(tmp,"");
                                            isFileExists = 2;
                                        }*/
                                        String aflag = "";
                                        if(tmp[0].equalsIgnoreCase("FIDATA")){
                                            aflag = tmp[5];
                                        }else if(tmp[0].equalsIgnoreCase("BASELINK")){
                                            aflag = tmp[4];
                                        }else if(tmp[0].equalsIgnoreCase("BASEDATA")){
                                            aflag = tmp[8];
                                        }
                                        String dir = "", sdir = "";                                                                             
                                        String chkDB = ChkUpdateInsert(tmp);
                                        if(!chkDB.contains("insert") && !chkDB.contains("update")){
                                            log.append("Database check failed for" + tmp + " in " + filepath ,  LogFilePath, "pdf_sort");
                                            System.exit(1);
                                        }
                                        boolean isfile = false;
                                        String pdf = "";;

                                        String [] fdtl = chkDB.split(",");
                                        if(aflag.trim().toUpperCase().equals("D")){
                                            //sf.delete();
                                            if(chkDB.contains("update")){
                                                delupdate(tmp,fdtl[2].trim());
                                            }else{
                                                CreateReturnCSV(tmp, "Error:Deleted document can not found");
                                            }
                                            isFileExists = 2;
                                        }else if(aflag.trim().toUpperCase().equals("U")){
                                            if(chkDB.contains("update")){
                                                if(tmp[0].equalsIgnoreCase("FIDATA")){
                                                    File udf = new File(CopyPath + fdtl[2].trim() + "\\" + fdtl[3].trim());
                                                    File rdf = new File(CopyPath + fdtl[2].trim() + "\\" + tmp[3]);
                                                    if(udf.exists()){
                                                        if(udf.renameTo(rdf)){
                                                            /*if(!fdtl[3].trim().toUpperCase().endsWith("PDF") && !fdtl[3].trim().toUpperCase().endsWith("HTM")){
                                                                if(ConvertToPDF(CopyPath + fdtl[2].trim() + "\\" + fdtl[3].trim(), CopyPath + fdtl[2].trim() + "\\")){
                                                                    update(tmp, fdtl[2].trim().replace("\\", "\\\\"), fdtl[3].trim().substring(0,fdtl[3].trim().indexOf(".")) + ".PDF");
                                                                }else{
                                                                    update(tmp, fdtl[2].trim().replace("\\", "\\\\"), "Could not convert to PDF");
                                                                }
                                                            }else{*/
                                                                update(tmp, fdtl[2].trim().replace("\\", "\\\\"), "");
                                                            //}

                                                        }else{
                                                            log.append("Could not rename document " + tmp[3] + " in " + CopyPath ,  LogFilePath, "pdf_sort");
                                                        }
                                                    }else{
                                                        CreateReturnCSV(tmp, "Error:Document can not found to rename");
                                                    }
                                                    udf = null;
                                                    rdf = null;
                                                }else if(tmp[0].equalsIgnoreCase("BASEDATA")){
                                                    File udf = new File(CopyPath + fdtl[2].trim() + "\\" + fdtl[3].trim());
                                                    File rdf = new File(CopyPath + fdtl[2].trim() + "\\" + tmp[6]);
                                                    if(udf.exists()){
                                                        if(udf.renameTo(rdf)){
                                                            /*if(!fdtl[3].trim().toUpperCase().endsWith("PDF") && !fdtl[3].trim().toUpperCase().endsWith("HTM")){
                                                                if(ConvertToPDF(CopyPath + fdtl[2].trim() + "\\" + fdtl[3].trim(), CopyPath + fdtl[2].trim() + "\\")){
                                                                    update(tmp, fdtl[2].trim().replace("\\", "\\\\"), fdtl[3].trim().substring(0,fdtl[3].trim().indexOf(".")) + ".PDF");
                                                                }else{
                                                                    update(tmp, fdtl[2].trim().replace("\\", "\\\\"), "Could not convert to PDF");
                                                                }
                                                            }else{*/
                                                                update(tmp, fdtl[2].trim().replace("\\", "\\\\"), "");
                                                            //}

                                                        }else{
                                                            log.append("Could not rename document " + tmp[6] + " in " + CopyPath ,  LogFilePath, "pdf_sort");
                                                        }
                                                    }else{
                                                        CreateReturnCSV(tmp, "Error:Document can not found to rename");
                                                    }
                                                    udf = null;
                                                    rdf = null;
                                                }else if(tmp[0].equalsIgnoreCase("BASELINK")){
                                                    update(tmp, "", "");
                                                }
                                                //String [] fdtl = chkDB.split(",");
                                                
                                            }else{
                                               CreateReturnCSV(tmp, "Error:Document can not found to rename");
                                            }
                                            isFileExists = 3;
                                        }else{

                                            for(int f=0;f<flist.size();f++){
                                                lbl_file.setText("Processing " + filepath + flist.get(f).toString());
                                                Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
                                                
                                                
                                                if(tmp[0].equalsIgnoreCase("FIDATA")){
                                                            File sf = new File(filepath + flist.get(f).toString());
                                                                if(tmp[3].equalsIgnoreCase(flist.get(f).toString())){
                                                                    if(sf.length() == Long.parseLong(tmp[4])){
                                                                        String [] subdir = tmp[3].split("_");
                                                                        if(subdir!=null){
                                                                            if(subdir[0].length() == 12){
                                                                                dir = subdir[0].substring(0,2);
                                                                                sdir = subdir[0].substring(10,12);
                                                                            }else{
                                                                                dir = "Cusip";
                                                                                sdir = subdir[0].substring(7,9);
                                                                            }
                                                                        }
                                                                        if(isDirExists(CopyPath + "\\" + dir + "\\" + sdir + "\\" + flist.get(f).toString())){
                                                                            File df = new File(CopyPath + "\\" + dir + "\\" + sdir + "\\" + flist.get(f).toString());
                                                                            if(!copyfile(sf, df)){
                                                                                log.append("Could not copy document " + flist.get(f).toString() + " in " + CopyPath ,  LogFilePath, "pdf_sort");
                                                                            }/*else{
                                                                                if(!flist.get(f).toString().toUpperCase().endsWith("PDF") && !flist.get(f).toString().toUpperCase().endsWith("HTM")){
                                                                                    if(ConvertToPDF(CopyPath + "\\" + dir + "\\" + sdir + "\\" + flist.get(f).toString(), CopyPath + "\\" + dir + "\\" + sdir + "\\")){
                                                                                        pdf = flist.get(f).toString().substring(0, flist.get(f).toString().indexOf(".")) + ".PDF";
                                                                                    }else{
                                                                                        pdf = "Could not convert to PDF";
                                                                                    }
                                                                                }else{
                                                                                    pdf = "";
                                                                                }
                                                                                isfile = true;
                                                                            }*/
                                                                            df = null;
                                                                        }
                                                                        /*if(isDirExists(ArchivePath + "\\" + dir + "\\" + sdir + "\\" + flist.get(f).toString())){
                                                                            File df = new File(ArchivePath + "\\" + dir + "\\" + sdir + "\\" + flist.get(f).toString());
                                                                            if(sf.renameTo(df)){                                                                    
                                                                                isfile = true;
                                                                            }else{
                                                                                log.append("Could not move document " + flist.get(f).toString() + " in " + ArchivePath ,  LogFilePath, "pdf_sort");
                                                                            }
                                                                            df = null;
                                                                        }*/
                                                                    }else{
                                                                        File df = new File(ReturnPath + "\\" + flist.get(f).toString());
                                                                        sf.renameTo(df);
                                                                        df = null;
                                                                        CreateReturnCSV(tmp, "Error: Document size discrepancy");
                                                                    }
                                                                    isFileExists = 1;
                                                                    break;
                                                                }
                                                            //}
                                                            sf = null;

                                                        }else if(tmp[0].equalsIgnoreCase("BASEDATA")){
                                                            File sf = new File(filepath + flist.get(f).toString());
                                                                if(tmp[6].equalsIgnoreCase(flist.get(f).toString())){
                                                                    if(sf.length() == Long.parseLong(tmp[7])){                                                                        
                                                                        
                                                                        dir = "BASE";                                                                            
                                                                        
                                                                        if(isDirExists(CopyPath + "\\" + dir + "\\" + flist.get(f).toString())){
                                                                            File df = new File(CopyPath + "\\" + dir + "\\" + flist.get(f).toString());
                                                                            if(!copyfile(sf, df)){
                                                                                log.append("Could not copy document " + flist.get(f).toString() + " in " + CopyPath ,  LogFilePath, "pdf_sort");
                                                                            }/*else{
                                                                                if(!flist.get(f).toString().toUpperCase().endsWith("PDF") && !flist.get(f).toString().toUpperCase().endsWith("HTM")){
                                                                                    if(ConvertToPDF(CopyPath + "\\" + dir + "\\" + flist.get(f).toString(), CopyPath + "\\" + dir + "\\")){
                                                                                        pdf = flist.get(f).toString().substring(0, flist.get(f).toString().indexOf(".")) + ".PDF";
                                                                                    }else{
                                                                                        pdf = "Could not convert to PDF";
                                                                                    }
                                                                                }else{
                                                                                    pdf = "";
                                                                                }
                                                                                isfile = true;
                                                                            }*/
                                                                            df = null;
                                                                        }
                                                                        /*if(isDirExists(ArchivePath + "\\" + dir + "\\" + flist.get(f).toString())){
                                                                            File df = new File(ArchivePath + "\\" + dir + "\\" + flist.get(f).toString());
                                                                            if(sf.renameTo(df)){                                                                    
                                                                                isfile = true;
                                                                            }else{
                                                                                log.append("Could not move document " + flist.get(f).toString() + " in " + ArchivePath ,  LogFilePath, "pdf_sort");
                                                                            }
                                                                            df = null;
                                                                        }*/
                                                                    }else{
                                                                        File df = new File(ReturnPath + "\\" + flist.get(f).toString());
                                                                        sf.renameTo(df);
                                                                        df = null;
                                                                        CreateReturnCSV(tmp, "Error: Document size discrepancy");
                                                                    }
                                                                    isFileExists = 1;
                                                                    break;
                                                                }
                                                            //}
                                                            sf = null;
                                                        }else if(tmp[0].equalsIgnoreCase("BASELINK")){
                                                            isfile = true;
                                                        }
                                            }
                                        }
                                        //enter in Database
                                      //  if(isfile){
                                            if(chkDB.contains("insert")){
                                                if(tmp[0].equalsIgnoreCase("FIDATA")){
                                                    insert(tmp, dir + "\\\\" + sdir, pdf);
                                                }else if(tmp[0].equalsIgnoreCase("BASEDATA")){
                                                    insert(tmp, dir, pdf);
                                                }else if(tmp[0].equalsIgnoreCase("BASELINK")){
                                                    insert(tmp, "", "");
                                                }
                                                
                                            }else if(chkDB.contains("update")){
                                                if(tmp[0].equalsIgnoreCase("FIDATA")){
                                                    String [] tsize = chkDB.split(",");
                                                    if(tsize[1].trim().equals(tmp[4])){
                                                        CreateReturnCSV(tmp, "Warrning: Document size is the same as the previous document");
                                                    }
                                                    update(tmp, dir + "\\\\" + sdir, pdf);
                                                }else if(tmp[0].equalsIgnoreCase("BASEDATA")){
                                                    String [] tsize = chkDB.split(",");
                                                    if(tsize[1].trim().equals(tmp[7])){
                                                        CreateReturnCSV(tmp, "Warrning: Document size is the same as the previous document");
                                                    }
                                                    update(tmp, dir, pdf);
                                                }else if(tmp[0].equalsIgnoreCase("BASELINK")){                                                    
                                                    update(tmp, "", "");
                                                }
                                            }
                                      //  }

                                        //File not found error
                                        if(isFileExists == 0){
                                            CreateReturnCSV(tmp, "Error: Document not found");
                                        }
                                    }
                                }
                                in.close();
                            //}
                             movesessionfile();
                        //}
                        ChkFinalFiles(filepath);
                    }
                    }
                }
                delFolders();
            }
           // }
             lbl_folder.setText("Finished Processing");
             Panel.paintImmediately(0,0,Panel.getWidth(), Panel.getHeight());
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"bt_clickActionPerformed: " + e.getMessage(),"bt_clickActionPerformed",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("bt_clickActionPerformed: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
    }//GEN-LAST:event_bt_clickActionPerformed

    private boolean ConvertToPDF(String sfile, String dpath){
        try{            
            //dpath = "C:\\test\\ecodata\\";
            if(!sfile.toUpperCase().endsWith("PDF") && !sfile.toUpperCase().endsWith("HTM")){
                File source = new File(sfile);
                if (source.exists()) {
                    connectToOffice();
                    // Build output file name.
                    String file_name = source.getName();
                    int dotIndex = file_name.lastIndexOf(".");
                    file_name = file_name.substring(0, dotIndex) + ".PDF";
                    File sFile = new File(dpath + "\\" + file_name);
                    if (sFile.exists() == false) {

                        // set source and destination file formats
                        DefaultDocumentFormatRegistry reg = new DefaultDocumentFormatRegistry();
                        DocumentFormat sourceFormat = reg.getFormatByFileExtension(file_name);
                        if (sourceFormat == null && file_name.endsWith("HTM")) {
                            sourceFormat = reg.getFormatByMimeType("text/html");
                        }
                        DocumentFormat destinFormat = reg.getFormatByMimeType("application/pdf");
                        DocumentConverter converter = new OpenOfficeDocumentConverter(ooConn);

                        // Try and do the conversion.
                        try {
                            converter.convert(source, sourceFormat, new File(dpath + file_name), destinFormat);
                        } catch (Exception e) {
                            log.append("converter: " + e.getMessage(),  LogFilePath, "pdf_sort");
                        }

                    } else {
                        log.append("File already exist as a PDF ",  LogFilePath, "pdf_sort");
                    }
                    ooConn.disconnect();
                    return true;
                } else {
                    log.append("Could not find the source file '" + sfile,  LogFilePath, "pdf_sort");
                    return false;                
                }            
            }else{
                return false;
            }
               
            
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"ConvertToPDF: " + e.getMessage(),"bt_clickActionPerformed",javax.swing.JOptionPane.ERROR_MESSAGE);               
            }else{
                log.append("ConvertToPDF: " + e.getMessage(),  LogFilePath, "pdf_sort");                
            }
            return false;
        }
    }
    
    private void connectToOffice() {        
        try {
            ooConn = new SocketOpenOfficeConnection();
            ooConn.connect();
        } catch (Exception e) {
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"connectToOffice: " + e.getMessage(),"bt_clickActionPerformed",javax.swing.JOptionPane.ERROR_MESSAGE);
            }else{
                log.append("connectToOffice: " + e.getMessage(),  LogFilePath, "pdf_sort");                
            }
        }
    }
    
    private boolean del_web_upload(String path){
        try{            
            String[] DirContents = null;
            File DirectoryListing = new File(path);
            DirContents = DirectoryListing.list();
            for (int x = 0; x<DirContents.length; x++){
                File sdirlst = new File(path + "\\" + DirContents[x]);
                String[] sdircon = sdirlst.list();
                for (int x1 = 0; x1<sdircon.length; x1++){
                    File fdirlst = new File(path + "\\" + DirContents[x] + "\\" + sdircon[x1]);
                    String[] fdircon = fdirlst.list();
                    for(int x2=0;x2<fdircon.length;x2++){
                        File dir = new File(path + "\\" + DirContents[x] + "\\" + sdircon[x1] + "\\" + fdircon[x2]);
                        dir.delete();
                        dir = null;
                    }
                    fdirlst.delete();
                    fdirlst = null;
                    fdircon = null;
                }
                sdirlst.delete();
                sdirlst = null;
                sdircon = null;
            }
            DirectoryListing = null;
            return true;
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"del_web_upload: " + e.getMessage(),"del_web_upload",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("del_web_upload: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
            return false;
        }
    }

    private void movesessionfile(){
        try{
            File msf = new File(masterfile);
            File mdf = new File(SessionPath + "\\" +  masterfilenm);
            msf.renameTo(mdf);
            mdf = null;
            msf = null;
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"movesessionfile: " + e.getMessage(),"movesessionfile",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("movesessionfile: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
    }

    private void CreateReturnCSV(String [] rec, String comment){
        try{
            String[] fname = masterfile.split("\\\\");
            String fnm = fname[fname.length-1].replace("PDF","BAD");
            BufferedWriter out = new BufferedWriter(new FileWriter(ReturnPath + "\\" + fnm, true));
            out.write(rec[0].trim() + ",\"" + rec[1].trim() + "\"," + rec[2].trim() + ",\"" + rec[3].trim() + "\",\"" + comment.trim() + "\"\n");
            out.close();
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"CreateReturnCSV: " + e.getMessage(),"CreateReturnCSV",javax.swing.JOptionPane.ERROR_MESSAGE);
                exitForm(Integer.parseInt(ErrCode));
            }else{
                log.append("CreateReturnCSV: " + e.getMessage(),  LogFilePath, "pdf_sort");
                exitForm(Integer.parseInt(ErrCode));
            }
        }
    }

    private boolean insert(String[] rec, String filepath, String pdf_file_name){
        try{
            if(rec[0].equalsIgnoreCase("FIDATA")){
                Statement ist = conn.createStatement();
                String [] fname = rec[3].split("_");
                String code = "",type = "",lang = "",SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);
                if(rec[6].equals("")) rec[6] = "null";
                if(rec[7].equals("")) rec[7] = "ENG";
                int seq = 0;
                String pdf_upload = "Not Uploaded";
                if(pdf_file_name.isEmpty() || pdf_file_name.equalsIgnoreCase("Could not convert to PDF")) pdf_upload = "";

                if(fname.length == 3){
                    code = fname[0];
                    type = fname[1];
                    lang = fname[2].substring(0, fname[2].indexOf("."));
                    seq= 0;
                }else if(fname.length == 4){
                    code = fname[0];
                    type = fname[1];
                    lang = fname[2];
                    //seq = Integer.parseInt(fname[3].replace(".PDF", ""));
                    seq = Integer.parseInt(fname[3].substring(0, fname[3].indexOf(".")));
                }
                System.out.println("INSERT INTO " + db + ".wfi(secid, actflag, acttime, file_path, file_name, file_size, code, type, lang, seq, session_file_date, doc_id, net_upload, com_upload, liquid_upload, doc_date, pdf_file_name, net_upload_pdf, liquid_upload_pdf, language) VALUES('" +
                                  rec[2] + "', 'I', '" + acttime + "', '" + filepath + "', '" + rec[3] + "', '" + rec[4] + "', '" + code + "', '" + type + "', '" + lang + "', '" + seq + "', '" + SessionDate + "', '" + rec[1] + "', 'Not Uploaded', 'Not Uploaded', 'Not Uploaded', " + rec[6] + ", '" + pdf_file_name + "', '" + pdf_upload + "', '" + pdf_upload + "', '" + rec[7] + "')");
                ist.executeUpdate("INSERT INTO " + db + ".wfi(secid, actflag, acttime, file_path, file_name, file_size, code, type, lang, seq, session_file_date, doc_id, net_upload, com_upload, liquid_upload, doc_date, pdf_file_name, net_upload_pdf, liquid_upload_pdf, language) VALUES('" +
                                  rec[2] + "', 'I', '" + acttime + "', '" + filepath + "', '" + rec[3] + "', '" + rec[4] + "', '" + code + "', '" + type + "', '" + lang + "', '" + seq + "', '" + SessionDate + "', '" + rec[1] + "', 'Not Uploaded', 'Not Uploaded', 'Not Uploaded', " + rec[6] + ", '" + pdf_file_name + "', '" + pdf_upload + "', '" + pdf_upload + "', '" + rec[7] + "')");
                ist.close();
                return true;
            }else if(rec[0].equalsIgnoreCase("BASEDATA")){
                Statement ist = conn.createStatement();
                String [] fname = rec[6].split("_");
                String code = "",type = "",lang = "",SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);
                if(rec[5].equals("")) rec[5] = "null";
                if(rec[10].equals("")) rec[10] = "ENG";
                int seq = 0;
                String pdf_upload = "Not Uploaded";
                if(pdf_file_name.isEmpty() || pdf_file_name.equalsIgnoreCase("Could not convert to PDF")) pdf_upload = "";

                if(fname.length == 2){
                    code = fname[0];                    
                    lang = fname[1].substring(0, fname[1].indexOf("."));
                    seq= 0;
                }else if(fname.length == 3){
                    code = fname[0];
                    lang = fname[1];
                    //seq = Integer.parseInt(fname[3].replace(".PDF", ""));
                    seq = Integer.parseInt(fname[2].substring(0, fname[2].indexOf(".")));
                }
                System.out.println("INSERT INTO " + db + ".basedata(actflag, acttime, file_path, file_name, file_size, code, lang, seq, session_file_date, basedocid, net_upload, com_upload, liquid_upload, doc_date, pdf_file_name, net_upload_pdf, liquid_upload_pdf, language, curencd, amount, programme, parent_id) VALUES(" +
                                  "'I', '" + acttime + "', '" + filepath + "', '" + rec[6] + "', '" + rec[7] + "', '" + code + "', '" + lang + "', '" + seq + "', '" + SessionDate + "', '" + rec[1] + "', 'Not Uploaded', 'Not Uploaded', 'Not Uploaded', " + rec[5] + ", '" + pdf_file_name + "', '" + pdf_upload + "', '" + pdf_upload + "', '" + rec[10] + "', '" + rec[2] + "', '" + rec[3] + "', '" + rec[4] + "', '" + rec[9] + "')");
                ist.executeUpdate("INSERT INTO " + db + ".basedata(actflag, acttime, file_path, file_name, file_size, code, lang, seq, session_file_date, basedocid, net_upload, com_upload, liquid_upload, doc_date, pdf_file_name, net_upload_pdf, liquid_upload_pdf, language, curencd, amount, programme, parent_id) VALUES(" +
                                  "'I', '" + acttime + "', '" + filepath + "', '" + rec[6] + "', '" + rec[7] + "', '" + code + "', '" + lang + "', '" + seq + "', '" + SessionDate + "', '" + rec[1] + "', 'Not Uploaded', 'Not Uploaded', 'Not Uploaded', " + rec[5] + ", '" + pdf_file_name + "', '" + pdf_upload + "', '" + pdf_upload + "', '" + rec[10] + "', '" + rec[2] + "', '" + rec[3] + "', '" + rec[4] + "', '" + rec[9] + "')");
                ist.close();
                return true;
            }else if(rec[0].equalsIgnoreCase("BASELINK")){
                Statement ist = conn.createStatement();
                String SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);
                
                System.out.println("INSERT INTO " + db + ".baselink(baselinkid, actflag, acttime, secid, basedocid, session_file_date) VALUES('" +
                                  rec[1] + "', 'I', '" + acttime + "', '" + rec[3] + "', '" + rec[2] + "', '" + SessionDate + "')");
                ist.executeUpdate("INSERT INTO " + db + ".baselink(baselinkid, actflag, acttime, secid, basedocid, session_file_date) VALUES('" +
                                  rec[1] + "', 'I', '" + acttime + "', '" + rec[3] + "', '" + rec[2] + "', '" + SessionDate + "')");
                ist.close();                
                return true;
            }else{
                return false;
            }
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"insert: " + e.getMessage(),"insert",javax.swing.JOptionPane.ERROR_MESSAGE);                
            }else{
                log.append("insert: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return false;
        }
    }

    private boolean update(String[] rec, String filepath, String pdf_file_name){
        try{
            if(rec[0].equalsIgnoreCase("FIDATA")){
                Statement ust = conn.createStatement();
                String [] fname = rec[3].split("_");
                String code = "",type = "",lang = "",SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);
                if(rec[6].equals("")) rec[6] = "null";
                if(rec[7].equals("")) rec[7] = "ENG";
                int seq = 0;
                String pdf_upload = "Not Uploaded";
                if(pdf_file_name.isEmpty() || pdf_file_name.equalsIgnoreCase("Could not convert to PDF")) pdf_upload = "";
                if(fname.length == 3){
                    code = fname[0];
                    type = fname[1];
                    lang = fname[2].substring(0, fname[2].indexOf("."));
                    seq= 0;
                }else if(fname.length == 4){
                    code = fname[0];
                    type = fname[1];
                    lang = fname[2];
                    //seq = Integer.parseInt(fname[3].replace(".PDF", ""));
                    seq = Integer.parseInt(fname[3].substring(0, fname[3].indexOf(".")));
                }
               ust.executeUpdate("UPDATE " + db + ".wfi SET secid  = '" + rec[2] + "', " +
                                  " actflag = 'U', " +
                                  " acttime  = '" + acttime + "', " +
                                  " file_path  = '" + filepath + "', " +
                                  " file_name  = '" + rec[3] + "', " +
                                  " file_size  = '" + rec[4] + "', " +
                                  " code  = '" + code + "', " +
                                  " type  = '" + type + "', " +
                                  " lang  = '" + lang + "', " +
                                  " seq  = '" + seq + "', " +
                                  " session_file_date  = '" + SessionDate + "', " +
                                  " net_upload  = 'Not Uploaded', " +
                                  " com_upload  = 'Not Uploaded', " +
                                  " liquid_upload  = 'Not Uploaded', " +
                                  " doc_date  = " + rec[6] + ", " +
                                  " net_upload_pdf  = '" + pdf_upload + "', " +
                                  " liquid_upload_pdf  = '" + pdf_upload + "', " +
                                  " language  = '" + rec[7] + "', " +
                                  " pdf_file_name  = '" + pdf_file_name + "' " +
                                  " WHERE doc_id = '" + rec[1] + "'");
                ust.close();
                return true;
            }else if(rec[0].equalsIgnoreCase("BASEDATA")){
                Statement ust = conn.createStatement();
                String [] fname = rec[6].split("_");
                String code = "",type = "",lang = "",SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);
                if(rec[5].equals("")) rec[5] = "null";
                if(rec[10].equals("")) rec[10] = "ENG";
                int seq = 0;
                String pdf_upload = "Not Uploaded";
                if(pdf_file_name.isEmpty() || pdf_file_name.equalsIgnoreCase("Could not convert to PDF")) pdf_upload = "";
                if(fname.length == 2){
                    code = fname[0];
                    lang = fname[1].substring(0, fname[1].indexOf("."));
                    seq= 0;
                }else if(fname.length == 3){
                    code = fname[0];
                    lang = fname[1];
                    //seq = Integer.parseInt(fname[3].replace(".PDF", ""));
                    seq = Integer.parseInt(fname[2].substring(0, fname[2].indexOf(".")));
                }
               ust.executeUpdate("UPDATE " + db + ".basedata SET " +
                                  " actflag = 'U', " +
                                  " acttime  = '" + acttime + "', " +
                                  " file_path  = '" + filepath + "', " +
                                  " file_name  = '" + rec[6] + "', " +
                                  " file_size  = '" + rec[7] + "', " +
                                  " code  = '" + code + "', " +                                  
                                  " lang  = '" + lang + "', " +
                                  " seq  = '" + seq + "', " +
                                  " curencd  = '" + rec[2] + "', " +
                                  " amount  = '" + rec[3] + "', " +
                                  " programme  = '" + rec[4] + "', " +
                                  " parent_id  = '" + rec[9] + "', " +
                                  " session_file_date  = '" + SessionDate + "', " +
                                  " net_upload  = 'Not Uploaded', " +
                                  " com_upload  = 'Not Uploaded', " +
                                  " liquid_upload  = 'Not Uploaded', " +
                                  " doc_date  = " + rec[5] + ", " +
                                  " net_upload_pdf  = '" + pdf_upload + "', " +
                                  " liquid_upload_pdf  = '" + pdf_upload + "', " +
                                  " language  = '" + rec[10] + "', " +
                                  " pdf_file_name  = '" + pdf_file_name + "' " +
                                  " WHERE basedocid = '" + rec[1] + "'");
                ust.close();
                return true;
            }else if(rec[0].equalsIgnoreCase("BASELINK")){
                Statement ust = conn.createStatement();                
                String SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);                
 
                System.out.println("UPDATE " + db + ".baselink SET secid  = '" + rec[3] + "', " +
                                  " actflag = 'U', " +
                                  " acttime  = '" + acttime + "', " +                                  
                                  " basedocid  = '" + rec[2] + "', " +
                                  " session_file_date  = '" + SessionDate + "'" +                                  
                                  " WHERE baselinkid = '" + rec[1] + "'");
                
                ust.executeUpdate("UPDATE " + db + ".baselink SET secid  = '" + rec[3] + "', " +
                                  " actflag = 'U', " +
                                  " acttime  = '" + acttime + "', " +                                  
                                  " basedocid  = '" + rec[2] + "', " +
                                  " session_file_date  = '" + SessionDate + "'" +                                  
                                  " WHERE baselinkid = '" + rec[1] + "'");
                
                
                ust.close();
                return true;
            }else{
                return false;
            }
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"update: " + e.getMessage(),"update",javax.swing.JOptionPane.ERROR_MESSAGE);
            }else{
                log.append("update: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return false;
        }
    }

    private boolean delupdate(String[] rec, String filepath){
        try{
            if(rec[0].equalsIgnoreCase("FIDATA")){
                Statement ust = conn.createStatement();
                String [] fname = rec[3].split("_");
                String code = "",type = "",lang = "",SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);
                if(rec[6].equals("")) rec[6] = "null";
                if(rec[7].equals("")) rec[7] = "ENG";
                int seq = 0;

                if(fname.length == 3){
                    code = fname[0];
                    type = fname[1];
                    lang = fname[2].substring(0, fname[2].indexOf("."));
                    seq= 0;
                }else if(fname.length == 4){
                    code = fname[0];
                    type = fname[1];
                    lang = fname[2];
                    //seq = Integer.parseInt(fname[3].replace(".PDF", ""));
                    seq = Integer.parseInt(fname[3].substring(0, fname[3].indexOf(".")));
                }
                ust.executeUpdate("UPDATE " + db + ".wfi SET secid  = '" + rec[2] + "', " +
                                  " actflag = 'D', " +
                                  " acttime  = '" + acttime + "', " +
                                  " file_path  = '" + filepath.replace("\\", "\\\\") + "', " +
                                  " file_name  = '" + rec[3] + "', " +
                                  " file_size  = '" + rec[4] + "', " +
                                  " code  = '" + code + "', " +
                                  " type  = '" + type + "', " +
                                  " lang  = '" + lang + "', " +
                                  " seq  = '" + seq + "', " +
                                  " doc_date  = " + rec[6] + ", " +
                                  " session_file_date  = '" + SessionDate + "'" +
                                  " WHERE doc_id = '" + rec[1] + "'");
                ust.close();
                return true;
            }else if(rec[0].equalsIgnoreCase("BASEDATA")){
                Statement ust = conn.createStatement();
                String [] fname = rec[6].split("_");
                String code = "",type = "",lang = "",SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);
                if(rec[5].equals("")) rec[5] = "null";
                if(rec[10].equals("")) rec[10] = "ENG";
                int seq = 0;

                if(fname.length == 2){
                    code = fname[0];                    
                    lang = fname[1].substring(0, fname[1].indexOf("."));
                    seq= 0;
                }else if(fname.length == 3){
                    code = fname[0];
                    lang = fname[1];
                    //seq = Integer.parseInt(fname[3].replace(".PDF", ""));
                    seq = Integer.parseInt(fname[2].substring(0, fname[2].indexOf(".")));
                }
                ust.executeUpdate("UPDATE " + db + ".basedata SET " +
                                  " actflag = 'D', " +
                                  " acttime  = '" + acttime + "', " +
                                  " file_path  = '" + filepath.replace("\\", "\\\\") + "', " +
                                  " file_name  = '" + rec[6] + "', " +
                                  " file_size  = '" + rec[7] + "', " +                                  
                                  " lang  = '" + lang + "', " +
                                  " seq  = '" + seq + "', " +
                                  " doc_date  = " + rec[5] + ", " +                                  
                                  " curencd  = " + rec[2] + ", " +
                                  " amount  = " + rec[3] + ", " +
                                  " programme  = " + rec[4] + ", " +
                                  " parent_id  = " + rec[9] + ", " +
                                  " session_file_date  = '" + SessionDate + "'" +
                                  " WHERE basedocid = '" + rec[1] + "'");
                ust.close();
                return true;
            }else if(rec[0].equalsIgnoreCase("BASELINK")){
                Statement ust = conn.createStatement();                
                String SessionDate = masterfilenm.substring(5, 9) + "-" +  masterfilenm.substring(9, 11) + "-" + masterfilenm.substring(11, 13);
                
                ust.executeUpdate("UPDATE " + db + "." + tbl + " SET secid  = '" + rec[3] + "', " +
                                  " actflag = 'D', " +
                                  " acttime  = '" + acttime + "', " +                                  
                                  " basedocid  = " + rec[2] + ", " +
                                  " session_file_date  = '" + SessionDate + "'" +
                                  " WHERE baselinkid = '" + rec[1] + "'");
                ust.close();
                return true;
            }else{
                return false;
            }
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"update: " + e.getMessage(),"update",javax.swing.JOptionPane.ERROR_MESSAGE);
            }else{
                log.append("update: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return false;
        }
    }

    private String ChkUpdateInsert(String[] rec){
        try{
            String rtn = "";
            if(rec[0].equalsIgnoreCase("FIDATA")){
                boolean isupdate = false;
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM " + db + ".wfi WHERE doc_id = '" + rec[1] + "'");
                if(rs!=null){
                    if(rs.next()){
                        rtn = "update, " + rs.getString("file_size") + ", " + rs.getString("file_path") + ", " + rs.getString("file_name");
                        isupdate = true;
                    }

                }

                if(!isupdate){
                    rtn = "insert, ";
                }
                stmt.close();
                rs.close();
            }else if(rec[0].equalsIgnoreCase("BASEDATA")){
                boolean isupdate = false;
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM " + db + ".basedata WHERE basedocid = '" + rec[1] + "'");
                if(rs!=null){
                    if(rs.next()){
                        rtn = "update, " + rs.getString("file_size") + ", " + rs.getString("file_path") + ", " + rs.getString("file_name");
                        isupdate = true;
                    }

                }

                if(!isupdate){
                    rtn = "insert, ";
                }
                stmt.close();
                rs.close();
            }else if(rec[0].equalsIgnoreCase("BASELINK")){
                boolean isupdate = false;
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM " + db + ".baselink WHERE baselinkid = '" + rec[1] + "'");
                if(rs!=null){
                    if(rs.next()){
                        rtn = "update,";
                        isupdate = true;
                    }

                }

                if(!isupdate){
                    rtn = "insert, ";
                }
                stmt.close();
                rs.close();
            }
            return rtn;
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"ChkUpdateInsert: " + e.getMessage(),"bt_clickActionPerformed",javax.swing.JOptionPane.ERROR_MESSAGE);
            }else{
                log.append("ChkUpdateInsert: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
            return "";
        }
    }

    private boolean isDirExists(String fname){
        try{
            String parent =  (new File(fname).getParent());
            System.out.println(parent);
            File dir = new File(parent);
            if(dir.exists()==false){
                return dir.mkdirs();
            }else{
                return true;
            }
        }catch(Exception e){
            return false;
        }
    }


    private void ChkFinalFiles(String fld){
        try{
            ArrayList chkfiles = new ArrayList();
            String[] DirContents = null;
            File DirectoryListing = new File(fld);
            DirContents = DirectoryListing.list();
            for (int x=0; x<DirContents.length; x++){
                DirContents[x] = DirContents[x].toUpperCase();
            }

            for (int x = 0; x<DirContents.length; x++){
                // Dose File start with filePrefix
                 if ( !DirContents[x].toUpperCase().endsWith("CSV"))
                     chkfiles.add(DirContents[x]);
            }
            if(chkfiles.size()>0){
                for(int f=0;f<chkfiles.size();f++){
                    File sf = new File(fld + chkfiles.get(f).toString());
                    long len = sf.length();
                    File df = new File(ReturnPath + "\\" +  chkfiles.get(f).toString());
                    sf.renameTo(df);
                    df = null;
                    sf = null;
                    String [] rtn = new String[4];
                    rtn[0] = "";
                    rtn[1] = chkfiles.get(f).toString();
                    rtn[2] = String.valueOf(len);
                    rtn[3] = "";
                    CreateReturnCSV(rtn, "Orphan Document");
                }
            }
        }catch(Exception e){
        }
    }

    private boolean getFiles(String fld){
        try{
            boolean rtn = false, mfile = false;
            String[] DirContents = null;            
            File DirectoryListing = new File(fld);
            DirContents = DirectoryListing.list();
            for (int x=0; x<DirContents.length; x++){
                DirContents[x] = DirContents[x].toUpperCase();
            }

            for (int x = 0; x<DirContents.length; x++){
                // Dose File start with filePrefix                
                 if ( DirContents[x].toUpperCase().endsWith("TXT") == true){
                     masterfile = fld + DirContents[x];
                     masterfilenm = DirContents[x];
                     mfile = true;
                 }else{
                     flist.add(DirContents[x]);
                 }
            }
            if(flist.size()>0 && mfile){
                rtn = true;
            }
	    return rtn;
        }catch(Exception e){
            return false;
        }
    }

    private boolean getFolders(String fld){
        try{
            boolean rtn = false;
            String[] DirContents = null;
            File DirectoryListing = new File(fld);
            DirContents = DirectoryListing.list();
            for (int x=0; x<DirContents.length; x++){
                DirContents[x] = DirContents[x].toUpperCase();
            }

            for (int x = 0; x<DirContents.length; x++){
                if(!DirContents[x].equalsIgnoreCase("bahar_return"))
                    fldlist.add(DirContents[x]);
            }
            if(fldlist.size()>0) rtn = true;
	    return rtn;
        }catch(Exception e){
            return false;
        }
    }

    private void delFolders(){
        try{
            for(int x=0;x<fldlist.size();x++){
                File dir = new File(SourcePath + "\\" + fldlist.get(x).toString());
                dir.delete();
                dir = null;
            }
        }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"delFolders: " + e.getMessage(),"delFolders",javax.swing.JOptionPane.ERROR_MESSAGE);
            }else{
                log.append("delFolders: " + e.getMessage(),  LogFilePath, "pdf_sort");
            }
        }
    }

    private boolean copyfile (File srfile, File dtfile){
         try{                                
                  InputStream in = new FileInputStream(srfile);
                  OutputStream out = new FileOutputStream(dtfile);
                  byte[] buf = new byte[1024];
                  int len;
                  while ((len = in.read(buf)) > 0){
                    out.write(buf, 0, len);
                  }
                  in.close();
                  out.close();
                  srfile = null;
                  dtfile = null;
                  return true;
         }catch(Exception e){
             log.append("copyfile: "+e.getMessage(),  LogFilePath, "pdf_sort");
             return false;
         }
     }

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
       new pdf_sort(args);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel;
    private javax.swing.JButton bt_click;
    private javax.swing.JLabel lbl_file;
    private javax.swing.JLabel lbl_folder;
    // End of variables declaration//GEN-END:variables

}
